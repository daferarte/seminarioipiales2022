from django.contrib import admin
from .models import Persona

# Register your models here.

class PersonaAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at',)
    search_fields = ('cedula','Nombre','Apellido')
    list_display = ('cedula','Nombre','Apellido', 'created_at')
    ordering = ('-created_at',)

    def save_model(self, request, obj, form, change):
        if not obj.user_id:
            obj.user_id = request.user.id
        obj.save()

admin.site.register(Persona, PersonaAdmin)