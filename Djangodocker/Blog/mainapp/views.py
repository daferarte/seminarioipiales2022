from django.shortcuts import render
from .models import Persona

# Create your views here.
def index(request):

    personas=Persona.objects.all()
    return render(request, 'mainapp/index.html', {
        'title': 'Home',
        'pagina':'Bienvenidos',
        'personas':personas
    })

def lista(request):

    persona=Persona.objects.get(pk=1)
    return render(request, 'mainapp/persona.html', {
        'title': 'Home',
        'pagina':'Bienvenidos',
        'persona':persona
    })
