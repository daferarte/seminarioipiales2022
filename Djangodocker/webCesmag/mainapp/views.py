from django.shortcuts import render, get_object_or_404
from blog.models import Category, Article, Location, Organizer
from .models import Content, LocationSite
from django.utils import timezone

# Create your views here.


def index(request):
    evento = get_object_or_404(Category, name="Eventos")
    noticia = get_object_or_404(Category, name="Noticias")
    eventos = Article.objects.filter(Categories=evento, dateFinishEvent__gt=timezone.now(), public=True)
    noticias = Article.objects.filter(Categories=noticia, dateFinishEvent__gt=timezone.now(), public=True)
    locationindex = get_object_or_404(LocationSite, pk=1)
    contenido = Content.objects.filter(LocationSite=locationindex, public=True).order_by('id')

    return render(request, 'mainapp/index.html', {
        'title': 'Universidad Cesmag',
        'articles': eventos,
        'noticias': noticias,
        'contenidos': contenido,
    })
