from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User

# Create your models here.


# clase categoria gestion de las categorias nombre descripcion y cuando fue creada.
class LocationSite(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    description = models.CharField(max_length=255, verbose_name='Descripcion', blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creado')

    class Meta:
        verbose_name = 'Ubicacion'
        verbose_name_plural = 'Ubicaciones'

    def __str__(self):
        return self.name


# clase Content maneja los contenidos del index de manera dinamica
class Content(models.Model):
    title = models.CharField(max_length=150, verbose_name='Titulo')
    contentizq = RichTextField(verbose_name='contenido izquierdo')
    image = models.ImageField(default='null', verbose_name="Imagen", upload_to="contenido")
    contentder = RichTextField(verbose_name='contenido derecha')
    public = models.BooleanField(verbose_name="¿Publicado?")
    user = models.ForeignKey(User, editable=False, verbose_name="Usuario", on_delete=models.PROTECT)  # clave relacional del modelo de usuarios de django
    LocationSite = models.ManyToManyField(LocationSite, verbose_name="Ubicacion", blank=True)  # clave relacional del modelo de organizador de django
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creado')
    update_at = models.DateTimeField(auto_now=True, verbose_name='Editado')

    class Meta:
        verbose_name = 'Contenido'
        verbose_name_plural = 'Contenidos'
        ordering = ['-created_at']

    def __str__(self):
        return self.title

    # esto es el final de la linea
# esto es el final de la linea
