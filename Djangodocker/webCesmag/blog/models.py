# Este modelo gestiona todo el blog de la universidad #eventos noticias etc
# para lo cual se importan 3 libreriaas
# modelos, ckeditor sirve para gestion de blogs y user para saber quien publica cada articulo

from django.db import models
from phone_field import PhoneField
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User

# Create your models here.


# clase categoria gestion de las categorias nombre descripcion y cuando fue creada.
class Category(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    description = models.CharField(max_length=255, verbose_name='Descripcion', blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creado')

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'

    def __str__(self):
        return self.name


# clase organizer gestion de organizadores de eventos
class Organizer(models.Model):

    name = models.CharField(max_length=100, verbose_name='Nombre')
    image = models.ImageField(default='null', verbose_name="Imagen", upload_to="organizer")
    description = models.CharField(max_length=254, verbose_name='Descripcion', blank=True)
    phonenumber = PhoneField(blank=True, help_text='Numero de contacto')
    mail = models.EmailField(max_length=254, verbose_name='Correo electronico', blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creado')

    class Meta:
        verbose_name = 'Organizador'
        verbose_name_plural = 'Organizadores'

    def __str__(self):
        return self.name


# clase location gestion de ubicacion de eventos
class Location(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    description = models.CharField(max_length=255, verbose_name='Descripcion', blank=True)
    ubicacion = models.CharField(max_length=255, verbose_name='Direccion', blank=True)
    mapa = models.CharField(max_length=255, verbose_name='Mapa', blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creado')

    class Meta:
        verbose_name = 'Ubicacion'
        verbose_name_plural = 'Ubicaciones'

    def __str__(self):
        return self.name


# clase document donde se crean varios documentos
class Document(models.Model):
    titulo = models.CharField(max_length=100, verbose_name='Titulo', default='null', null=False)
    archivo = models.FileField(upload_to="archivos", default='null', null=True, blank=True)


# Clase articulos donde se referencia el titulo, contenido del articulo, imagen destacada
# si es publico o oculto, usuario que publica, hereda categorias, cuando fue creado y modificado
class Article(models.Model):
    title = models.CharField(max_length=150, verbose_name='Titulo')
    content = RichTextField(verbose_name='contenido')
    dateStartEvent = models.DateField(auto_now=False, verbose_name='Fecha inicio')
    timeStartEvent = models.TimeField(auto_now=False, verbose_name='hora inicio')
    dateFinishEvent = models.DateField(auto_now=False, verbose_name='Fecha termino', blank=True)
    timeFinishEvent = models.TimeField(auto_now=False, verbose_name='hora inicio', blank=True)
    image = models.ImageField(default='null', verbose_name="Imagen", upload_to="articles")
    public = models.BooleanField(verbose_name="¿Publicado?")
    Document = models.ManyToManyField(Document, verbose_name="Documentos", blank=True)
    user = models.ForeignKey(User, editable=False, verbose_name="Usuario", on_delete=models.PROTECT)  # clave relacional del modelo de usuarios de django
    Organizer = models.ManyToManyField(Organizer, verbose_name="Organizadores", blank=True)  # clave relacional del modelo de organizador de django
    Location = models.ManyToManyField(Location, verbose_name="Ubicacion", blank=True)  # relacion de 1 a muchos un articulo puede tener muchas categorias
    Categories = models.ManyToManyField(Category, verbose_name="categorías", blank=True)  # relacion de 1 a muchos un articulo puede tener muchas categorias
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creado')
    update_at = models.DateTimeField(auto_now=True, verbose_name='Editado')

    class Meta:
        verbose_name = 'Articulo'
        verbose_name_plural = 'Articulos'
        ordering = ['-created_at']

    def __str__(self):
        return self.title
