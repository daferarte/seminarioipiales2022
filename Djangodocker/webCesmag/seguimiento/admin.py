from django.contrib import admin
from .models import UserQuery


# clase UserQueryAdmin sirve para ordenar los usuarios que escriben a la plataforma
class UserQueryAdmin(admin.ModelAdmin):

    readonly_fields = ('created_at', )
    search_fields = ('name', 'program')
    # list_filter = ('visible',)
    list_display = ('name', 'created_at')
    ordering = ('-created_at', )


# Register your models here.
admin.site.register(UserQuery, UserQueryAdmin)
