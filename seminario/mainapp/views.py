from django.shortcuts import render
from .models import Persona

# Create your views here.

def index(request):
    personas= Persona.objects.all()
    persona1= Persona.objects.filter(pk=1)
    return render(request, 'mainapp/index.html',{
        'title': 'Uniremington',
        'personas':personas,
        'persona':persona1
    })