from django.contrib import admin
from .models import Persona

# Register your models here.

class PersonaAdmin(admin.ModelAdmin):
    readonly_fields = ('create_at', )
    search_fields = ('cedula', 'nombres', 'apellidos', 'user')
    list_display = ('cedula', 'nombres', 'apellidos', 'user')
    ordering= ('-create_at', )

    def save_model(self, request, obj, form, change):
        if not obj.user_id:
            obj.user_id= request.user.id
        obj.save()

admin.site.register(Persona, PersonaAdmin)