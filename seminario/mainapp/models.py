from tabnanny import verbose
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Persona(models.Model):
    cedula=models.CharField(max_length=15, verbose_name='Cedula')
    nombres=models.CharField(max_length=50, verbose_name='Nombres')
    apellidos=models.CharField(max_length=50, verbose_name='Apellidos')
    direccion=models.CharField(max_length=50, verbose_name='Dirección')
    genero=models.BooleanField(verbose_name='¿Genero masculino?')
    user= models.ForeignKey(User, editable=False, verbose_name='Usuario', on_delete=models.PROTECT)
    create_at=models.DateTimeField(auto_now_add=True, verbose_name='creado')
    update_at= models.DateTimeField(auto_now=True, verbose_name='Editado')

    class Meta:
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'
        ordering= ['-create_at']

    def __str__(self):
        return self.cedula